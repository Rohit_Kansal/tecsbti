from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.
class faqModel(models.Model):
    ques=models.CharField(max_length=500)
    ans=models.CharField(max_length=2000)

def validate_image_message(image):
    size = image.file.size 
    if(size>(300*300)):
        raise ValidationError("Height or Width is larger than what is allowed(only 300*300)")

class messageModel(models.Model):
    m_name=models.CharField(max_length=50)
    about=models.CharField(max_length=100)
    message=models.CharField(max_length=1000)
    cover=models.ImageField('Image',upload_to="images/",validators=[validate_image_message],help_text='Image Size: 300 * 300')
    def __str__(self):
        return self.m_name

def validate_image(image):
    size = image.file.size 
    if(size>(600*600)):
        raise ValidationError("Height or Width is larger than what is allowed(only 600*600)")

class advisoryModel(models.Model):
    mbr_name=models.CharField(max_length=50)
    about=models.CharField(max_length=100)
    cover=models.ImageField('Image',upload_to="images/",validators=[validate_image],help_text='Image Size: 600 * 600')

    def __str__(self):
        return self.mbr_name

class smcModel(models.Model):
    mbr_name=models.CharField(max_length=50)
    designation=models.CharField(max_length=50)
    cover=models.ImageField('Image',upload_to="images/",validators=[validate_image],help_text='Image Size: 600 * 600')
    def __str__(self):
        return self.mbr_name

class teamModel(models.Model):
    id = models.AutoField(primary_key=True)
    mbr_name=models.CharField(max_length=50)
    fname=models.CharField(max_length=50)
    designation=models.CharField(max_length=50)
    qualification=models.CharField(max_length=50)
    
    def __str__(self):
        return self.mbr_name

class cbseModel(models.Model):
    doc_name=models.CharField(max_length=50)
    doc=models.FileField('pdf',upload_to="images/")
    def __str__(self):
        return self.doc_name



def validate_image_galley(image):
    size = image.file.size 
    #print(height)
    #width = image.file.width
    if(size>(1024*1024)):
        raise ValidationError("Height or Width is larger than what is allowed(only 1024*1024)")

class sportsModel(models.Model):
    game_name=models.CharField(max_length=50)
    cover=models.ImageField('Image',upload_to="images/",validators=[validate_image_galley],help_text='Image Size: 1MB')
    def __str__(self):
        return self.game_name

class galleryModel(models.Model):
    pic_title=models.CharField(max_length=50)
    pic_discription=models.CharField(max_length=100)
    session=models.CharField(max_length=10,default='')
    pic_category=models.CharField(max_length=20)
    pic=models.ImageField('Image',upload_to="images/",validators=[validate_image_galley],help_text='Image Size: 1MB')

    def __str__(self):
        return self.pic_title
