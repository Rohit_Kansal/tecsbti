from django.contrib import admin
from app_admin import models
# Register your models here.


class faqModelAdmin(admin.ModelAdmin):
    list_display=['ques','ans']

class advisoryModelAdmin(admin.ModelAdmin):
    list_display=('mbr_name','about','cover')

class galleryModelAdmin(admin.ModelAdmin):
    list_display=('pic_title','pic_discription','session','pic_category','pic')

class messageModelAdmin(admin.ModelAdmin):
    list_display=('m_name','about','message','cover')

class cbseModelAdmin(admin.ModelAdmin):
    list_display=('doc_name','doc')

class smcModelAdmin(admin.ModelAdmin):
    list_display=('mbr_name','designation','cover')

class teamModelAdmin(admin.ModelAdmin):
    list_display=('id','mbr_name','fname','designation','qualification')

class sportsModelAdmin(admin.ModelAdmin):
    list_display=('game_name','cover')


admin.site.register(models.sportsModel,sportsModelAdmin)
admin.site.register(models.smcModel,smcModelAdmin)
admin.site.register(models.galleryModel,galleryModelAdmin)
admin.site.register(models.advisoryModel,advisoryModelAdmin)
admin.site.register(models.faqModel,faqModelAdmin)
admin.site.register(models.messageModel,messageModelAdmin)
admin.site.register(models.teamModel,teamModelAdmin)
admin.site.register(models.cbseModel,cbseModelAdmin)
