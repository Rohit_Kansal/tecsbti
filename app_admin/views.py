from django.shortcuts import render
from django.core.mail import send_mail
from django.http import JsonResponse
from app_admin import models
from django.views.generic import View

# Create your views here.
def call_index_view(request):
    if(request.method=='POST'):
        name=request.POST.get('name')
        subject=request.POST.get('subject')
        message=request.POST.get('message')
        fromEmail=request.POST.get('email')
        contact=request.POST.get('contact')
        toEmail='ceotecs22@gmail.com'
        message= "Client Name is :  "+name+" , contact number is : "+contact + " and message is : "+message
        print(message)
        print(fromEmail)
        print(toEmail)
        try:
            send_mail(subject,message,fromEmail,[toEmail])
            #print(request.POST)
            msg='OK'
            data = {
                'msg': msg
                }
            return JsonResponse(data,status=200)
        except Exception as ex:
            msg=ex
            data = {
                'msg': msg
                }
            return JsonResponse(data,status=404)
    else:
        obj=models.faqModel.objects.all()
        objAd=models.advisoryModel.objects.all()
        objMsg=models.messageModel.objects.all()
        return render(request,"app_admin/index.html",{'faq':obj,'adv':objAd,'objMsg':objMsg})

def call_about_school(request):
    return render(request,"app_admin/about_school.html")
def call_SMC(request):
    smc=models.smcModel.objects.all()
    return render(request,"app_admin/SMC.html",{'smc':smc})
def call_academicTeam(request):
    team=models.teamModel.objects.all()
    return render(request,"app_admin/AcademicTeam.html",{'team':team})
def call_cbse_info(request):
    obj=models.cbseModel.objects.all()
    for i in obj:
        print(i.doc_name)
        print(i.doc.url)
    return render(request,"app_admin/cbse_info.html",{'obj':obj})

def call_admission_view(request):
    return render(request,"app_admin/admission.html")

def call_academicSession_view(request):
    return render(request,"app_admin/academic_session.html")

def call_ekl_rattan_view(request):
    return render(request,"app_admin/rattan.html")

def call_book_view(request):
    return render(request,"app_admin/book_list.html")

def call_sports_view(request):
    sports=models.sportsModel.objects.all()
    return render(request,"app_admin/sports.html",{'sports':sports})

def call_gallery_view(request):
    pic_cat=models.galleryModel.objects.values_list('pic_category',flat=True).distinct()
    print(pic_cat)
    pictureObj=models.galleryModel.objects.all()
    print("..........................")
    return render(request,"app_admin/gallery.html",{'pic_cat':pic_cat,'pictureObj':pictureObj})

class DisclosureView(View):
    template_name = "app_admin/disclosure_info.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)