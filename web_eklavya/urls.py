"""web_eklavya URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path
from app_admin import views
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.call_index_view,name='index'),
    path('school',views.call_about_school,name='school'),
    path('smc',views.call_SMC,name='smc'),
    path('team',views.call_academicTeam,name='team'),
    path('cbse',views.call_cbse_info,name='cbse'),
    path('admission/',views.call_admission_view,name='admission'),
    path('academic/',views.call_academicSession_view,name='academic'),
    path('books/',views.call_book_view,name='book'),
    path('award/',views.call_ekl_rattan_view,name='award'),
    path('sports/',views.call_sports_view,name='sports'),
    path('gallery/',views.call_gallery_view,name='gallery'),
    path('mandatory_public_disclosure_info/',views.DisclosureView.as_view() ,name='disclosure'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)